
# libraries ---------------------------------------------------------------

library("DDoutlier")
library("dplyr")
library("forcats")
library("ggplot2")
library("purrr")
library("readr")
library("skimr")
library("tibble")
library("tidyr")
library("tidyselect")
library("yardstick")


# loading of data ---------------------------------------------------------
# data downloaded from: https://www.kaggle.com/uciml/aps-failure-at-scania-trucks-data-set

workset <- read_csv(file = "/data/local/data/scania_failures/aps_failure_training_set.csv", 
                    na = "na", 
                    col_types = cols(.default = col_double(), 
                                     class = col_character())) %>% 
  mutate(class = factor(class, 
                        levels = c("neg", "pos"),
                        labels = c("no_failure", "failure"),
                        ordered = TRUE)) %>% 
  rename(outcome = class)

# using skimr
(summary_stats <- skim(workset))

# recode variables with too many missing values --------------------------

# get column names of variables with > N missing values
missing_treshold <- 1000
colnames_missing_values <- summary_stats %>% filter(n_missing >= missing_treshold) %>% pull(skim_variable)

#  helper functions
fun_recode_missing <- function(input) {
  # if value in input data is missing, replace with true
  return(if_else(is.na(input), 
                 true = TRUE, 
                 false = FALSE))
}

fun_new_name_missing <- function(input) {
  return(paste0(input, "_is_missing"))
}

# do the recoding
workset <- workset %>% 
  mutate(across(all_of(colnames_missing_values), 
         fun_recode_missing)) %>% 
  rename_with(fun_new_name_missing,
              all_of(colnames_missing_values))

# check effects using skimr again
(summary_stats <- skim(workset))

# remove remaining rows with missing values -------------------------------

# check what is the proportion of failures in the dataset with less than 1000 missing values
# included
workset %>% 
  filter(complete.cases(.)) %>% 
  count(outcome) %>% 
  mutate(`n%` = 100*n / sum(n))

# excluded
workset %>% 
  filter(!complete.cases(.)) %>% 
  count(outcome) %>%
  mutate(`n%` = 100*n / sum(n))
# So, there's a substantial difference in proportion of failures in the excluded
# rows. So there's possibly some information in the 'missingnes' of the data.
# However, numbers are low. Let's first prioritize the large chunk of data
# without missing values
workset <- workset %>% 
  filter(complete.cases(.))

# remove variables with (near) zero variance ------------------------------
  
zero_variance_info <- caret::nearZeroVar(workset, 
                                         saveMetrics = TRUE) %>% 
  # make sure the outcome variable is not excluded
  filter(rownames(.) != "outcome")

# get names of variables with zero variance
zerv_variance_variables <- zero_variance_info %>% 
  filter(zeroVar == TRUE) %>% 
  rownames()

# and filter them out
workset <- workset %>% 
  select(-all_of(zerv_variance_variables))

# recompute near zero variance statistics for the numeric variables only
zero_variance_info <- workset %>% 
  select(where(is.numeric)) %>% 
  caret::nearZeroVar(freqCut = 99/1,
                     uniqueCut = 5,
                     saveMetrics = TRUE) %>% 
  filter(rownames(.) != "outcome")

# take a look at the near zero variance variables
near_zerv_variance_variables <- zero_variance_info %>% 
  filter(nzv == TRUE) %>% 
  rownames()

# add info (number of distinct rows) to the default skim function
custom_skim <- skim_with(numeric = sfl(n_distinct), 
                         append = TRUE)

(summary_stats <- workset %>% 
  select(all_of(near_zerv_variance_variables)) %>% 
  custom_skim())

# convert numerical values with near zero variance to logicals
fun_recode_some_value <- function(input) {
  # if value in input data has a value > 0, replace with true
  return(if_else(input > 0, 
                 true = TRUE, 
                 false = FALSE))
}

fun_new_name_some_value <- function(input) {
  # append _had_some_value to the variable name
  return(paste0(input, "_had_some_value"))
}

# do the recoding
colnames_some_value <- summary_stats %>% pull(skim_variable)

workset <- workset %>% 
  mutate(across(all_of(colnames_some_value), 
                fun_recode_some_value)) %>% 
  rename_with(fun_new_name_some_value,
              all_of(colnames_some_value))

(summary_stats <- skim(workset))

# do another near zero variance check
zero_variance_info <- workset %>% 
  caret::nearZeroVar(freqCut = 99/1,
                     uniqueCut = 5,
                     saveMetrics = TRUE) %>% 
  filter(rownames(.) != "outcome")

# check variables with remaining near zero variance
near_zerv_variance_variables <- zero_variance_info %>% 
  filter(nzv == TRUE) %>% 
  rownames()

skim(select(workset, all_of(near_zerv_variance_variables)))

# remove the remaining near zero variance variables
workset <- workset %>% 
  select(-all_of(near_zerv_variance_variables))

# normalize data ----------------------------------------------------------
# helper function
# TODO: eventually, make sure that for 'new' data the same min(input) and
#       max(input) are used by storing them
fun_normalize <- function(input) {
  return((input - min(input)) / (max(input) - min(input)))
}

workset <- workset %>% 
  mutate(across(where(is.numeric), fun_normalize))

# skim(workset)

# do some cleanup of the workspace
rm(summary_stats, zero_variance_info, colnames_missing_values, colnames_some_value,
   missing_treshold, near_zerv_variance_variables, zerv_variance_variables,
   custom_skim, fun_new_name_missing, fun_new_name_some_value, fun_recode_missing, 
   fun_recode_some_value)

# outlier detection -------------------------------------------------------
# Do actual outlier detection based on a selection from: http://doi.org/10.1371/journal.pone.0152173

# knn - global anomaly detection
outlier_score_knn_agg <- KNN_AGG(select(workset, -outcome), k_min = 5, k_max = 10)

# lof - local outlier factor
outlier_score_lof <- LOF(select(workset, -outcome), k = 5)

# sometimes scores are infinite, replace with max
outlier_score_lof <- if_else(condition = outlier_score_lof == Inf,
                             true = max(outlier_score_lof),
                             false = outlier_score_lof)
  
# add outlier scores to tibble
workset <- workset %>% 
  add_column(outlier_knn = outlier_score_knn_agg,
             outlier_lof = outlier_score_lof,
             .after = "outcome")

rm(outlier_score_knn_agg, outlier_score_lof)

# Inspect outlier scores --------------------------------------------------

# so, any difference in knn outlier score for different outcome?
t.test(outlier_knn ~ outcome, 
       data = workset, 
       alternative = "less", 
       conf.level = 0.95)
# well, yes there is

workset %>% 
  group_by(outcome) %>% 
  summarise(mean = mean(outlier_lof, na.rm = TRUE))

t.test(outlier_lof ~ outcome, 
       data = workset, 
       alternative = "less", 
       conf.level = 0.95)
# LOF doesn't seem to work *in this dataset* and/or *with these variables*

# plot outlier scores
(overall_plot <- workset %>% 
  ggplot(aes(x = outlier_knn, 
             color = outcome, 
             fill = outcome)) +
  geom_histogram(bins = 200))

# zoomed in on plot
# note that the 7 highest scoring observations are 'no failures'
(zoom_plot <- overall_plot +
  coord_cartesian(ylim = c(0, 50)))

# effect of outlier cut off on hit rate and found failures ----------------

# set cut off values of interest
cut_offs <-c(seq(from = floor(max(workset$outlier_knn)), to = 5, by = -5), 1)

# create a list of tibbles to use purrr functionality
workset_list <- workset %>% 
  select(outcome, outlier_knn) %>% 
  list() %>% 
  rep(times = length(cut_offs))

# helper function
function_add_cut_off_column <- function(df_input, cut_off_value) {
  # this function adds the classification according to the cut off value of the
  # outlier score to the results tibble in the lists
  df_output <- df_input %>%
    mutate(flagged = if_else(outlier_knn >= cut_off_value,
                             true = "failure",
                             false = "no_failure"),
           flagged = as_factor(flagged),
           cut_off = cut_off_value,
           .after = outlier_knn)

  return(df_output)
}

# apply cut off value for each cut off value
workset_list <- map2(workset_list, 
                     cut_offs, 
                     ~function_add_cut_off_column(.x, .y))

# create a nested tibble
workset_list <- workset_list %>% 
  bind_rows() %>% 
  group_by(cut_off) %>% 
  nest()


# create confusion matrix for each cut off point --------------------------

# add confusion matrix to each cut off value in the nested tibble
workset_list <- workset_list %>% 
  mutate(conf_mat = map(data, ~caret::confusionMatrix(.x$flagged,
                                                      .x$outcome,
                                                      positive = "failure")))

# extract positive predictive accuracy and sensitivity -------------------

# extract some key metrics from the confusion metrics at each cut off value
# Note that the classical trade off between sensitivity and specificity is not
# necessarily useful in the context of outlier detection. This depends, naturally,
# on the specific business requirements. Other choices can be mad
# 
# sensitivity: what % of actual failures are detected
# ppv: positive predictive value - if classified as a failure, what % is actually
#                                  a failure
workset_list <- workset_list %>% 
  mutate(sensitivity = map_dbl(conf_mat, c("byClass", "Sensitivity")),
         ppv = map_dbl(conf_mat, c("byClass", "Pos Pred Value")))

# Make a plot to visualize impact of cut off point on sensitivity and ppv
# 
# TODO: work on visual results. Perhaps add cut-off value or % of cases to each
#       point on the graph to aid business in understanding impact of cut off 
#       value number of checked cases
# 
# obviously, labelling of axes if not very helpfull, but easy to fix in a real 
# application :-)
workset_list %>% 
  ggplot(aes(x = sensitivity,
             y = ppv)) +
  geom_point() +
  geom_line()


# TODO --------------------------------------------------------------------

#' TODO: feature selection/feature sensitivity
#' TODO: vary and check effects of changing k_min, k_max, and k
#' TODO: work on visual results. Perhaps add cut-off value or % of cases to each
#'       point on the graph to aid business in understanding impact of cut off 
#'       value number of checked cases
#' TODO: eventually, make sure that for 'new' data the same min(input) and
#'       max(input) are used by storing them